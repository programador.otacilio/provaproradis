<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(array('prefix' => 'api'), function () {    
    Route::group(array('prefix' => 'fabricante'), function () {
        Route::get('', 'FabricanteController@index');
        Route::post('', 'FabricanteController@store');
        Route::get('{id}', 'FabricanteController@show');
        Route::put('{id}', 'FabricanteController@update');
        Route::delete('{id}', 'FabricanteController@destroy');    
    });
    Route::group(array('prefix' => 'vacina'), function () {
        Route::get('', 'VacinaController@index');
        Route::post('', 'VacinaController@store');
        Route::get('{id}', 'VacinaController@show');
        Route::put('{id}', 'VacinaController@update');
        Route::delete('{id}', 'VacinaController@destroy');    
    });
    Route::group(array('prefix' => 'paciente'), function () {
        Route::get('', 'PacienteController@index');
        Route::post('', 'PacienteController@store');
        Route::get('{id}', 'PacienteController@show');
        Route::put('{id}', 'PacienteController@update');
        Route::delete('{id}', 'PacienteController@destroy');    
    });
    Route::group(array('prefix' => 'vacinacao'), function () {
        Route::get('', 'VacinacaoController@index');
        Route::post('', 'VacinacaoController@store');
        Route::get('{id}', 'VacinacaoController@show');
        Route::put('{id}', 'VacinacaoController@update');
        Route::delete('{id}', 'VacinacaoController@destroy');    
    });
});