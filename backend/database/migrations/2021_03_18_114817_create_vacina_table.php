<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacinaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_vacina', function (Blueprint $table) {
            $table->increments('co_vacina');
            $table->integer('co_fabricante');
            $table->string('ds_lote', 50);
            $table->date('dt_validade');
            $table->integer('nu_dose');
            $table->integer('nu_dias_intervalo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_vacina');
    }
}
