<?php

use Illuminate\Database\Seeder;

class FabricanteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('tb_fabricante')->delete();

        if(!DB::table('tb_fabricante')->count()){
            DB::table('tb_fabricante')->insert( array(
                array('no_nome' => 'AstraZeneca/Oxford','created_at' => new DateTime, 'updated_at' => new DateTime),
                array('no_nome' =>  'Covax Facility','created_at' => new DateTime, 'updated_at' => new DateTime),
                array('no_nome' =>  'Pfizer/BioNTech','created_at' => new DateTime, 'updated_at' => new DateTime),
                array('no_nome' =>  'Janssen','created_at' => new DateTime, 'updated_at' => new DateTime),
                array('no_nome' =>  'CoronaVac','created_at' => new DateTime, 'updated_at' => new DateTime),
                array('no_nome' =>  'Sputnik','created_at' => new DateTime, 'updated_at' => new DateTime)
            ));
        }
    }
}
