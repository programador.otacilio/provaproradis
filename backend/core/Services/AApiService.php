<?php

namespace Core\Services;

use App\Modules\Arquivo\Models\Fase as FaseModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

abstract class AApiService implements IService
{
    protected $model;

    protected $itemsPerPage = 10;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function getModel(): ?Model
    {
        return $this->model;
    }

    public function getUrlDiretorio($co_fase_arquivo, $co_tipo_arquivo)
    {
        $fase = FaseModel::find($co_fase_arquivo);
        $tipoArquivo = $fase->tiposArquivos()->find($co_tipo_arquivo);
        return date('Y') . "/$fase->no_fase_arquivo/$tipoArquivo->no_tipo_arquivo";
    }

    public function formatPaginateParams(Request $request, $sort = null, $order = '') {
        $order = ($request->sortDesc && ($request->sortDesc[0] === 'true')) ? '-' : $order;
        $sort = !empty($request->sortBy) ? $request->sortBy[0] : $sort;
        $sort = str_replace('.', '_', $sort);
        $request->merge(['sort' => "{$order}{$sort}"]);

        $this->itemsPerPage = $request->itemsPerPage ? $request->itemsPerPage : -1;
    }
}
