<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Request;
class DoseMaxima implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $totalVacinaPaciente =  app()->make(\App\Services\Vacinacao::class)->getNumeroDosePacienteVacina($value,Request::get('co_vacina'));
        $vacina =  app()->make(\App\Services\Vacina::class)->buscar(Request::get('co_vacina'));
        
        if($totalVacinaPaciente < $vacina->nu_dose){
            return true; 
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Dose Máxima permitida';
    }
}
