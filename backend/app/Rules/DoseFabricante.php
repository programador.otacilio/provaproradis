<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Request;
class DoseFabricante implements Rule
{
    
    public function passes($attribute, $value)
    {
        $ultimaVacina =  app()->make(\App\Services\Vacinacao::class)->getUltimaDosePacienteVacina($value);
        if(!$ultimaVacina){
            return true;
        }

        return $ultimaVacina->co_vacina == Request::get('co_vacina');
    }

    
    public function message()
    {
        return 'Cliente já possui registro em outra Vacina';
    }
}
