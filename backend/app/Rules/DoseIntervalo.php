<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Request;
use Carbon\Carbon;
class DoseIntervalo implements Rule
{

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    private $dias;
    public function passes($attribute, $value)
    {
        $ultimaVacina =  app()->make(\App\Services\Vacinacao::class)->getUltimaDosePacienteVacina($value);
        if(!$ultimaVacina){
            return true;
        }

        $vacina =  app()->make(\App\Services\Vacina::class)->buscar(Request::get('co_vacina'));
        $date = Carbon::createFromFormat('Y-m-d', $ultimaVacina->dt_vacinacao);
        $intervalo = $date->diffInDays(Request::get('dt_vacinacao'))+1;
        $this->dias = $vacina->nu_dias_intervalo; 
        if($intervalo < $vacina->nu_dias_intervalo){
            return false;
        }
        
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Deve se esperar o prazo de '.$this->dias.' dias entre uma vacina e outra';
    }
}
