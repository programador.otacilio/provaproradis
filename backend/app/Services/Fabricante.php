<?php
namespace App\Services;
use App\Exceptions\ValidacaoCustomizadaException;
use Core\Services\AApiService;
use Illuminate\Http\Response;
use App\Models\Fabricante as FabricanteModel;
use Validator;
use DB;

class Fabricante extends AApiService
{
    public function __construct(FabricanteModel $model)
    {
        parent::__construct($model);
    }

    public function todos(){
        return $this->getModel()->all();
    }

    public function checkExistById($id){
        $model =  $this->getModel()->where('co_vacina',$id);
        return $model->count() ? true : false;
    }

    public function inserir($request)
    {
        try {
            DB::beginTransaction();
            $result = $this->getModel()->create($request->all());
            DB::commit();
            return $result;
        }  catch (\HttpException $queryException) {
            DB::rollBack();
            throw $queryException;
        }
    }
    public function buscar($id)
    {
        return $this->getModel()->find($id);
    }
    public function atualizar($request,$id)
    {
        try {
            DB::beginTransaction();
            $model = $this->getModel()->find($id);
            if(!$model) {
                throw new ValidacaoCustomizadaException(
                    'Registro não encontrado',
                    Response::HTTP_NOT_FOUND
                );
            }
            $model->fill($request->all());
            $model->save();
            DB::commit();

            return $model;

        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

       
    }
    public function deletar($id)
    {
        try {
            $model = $this->getModel()->find($id);
            if(!$model) {
                throw new ValidacaoCustomizadaException(
                    'Registro não encontrado',
                    Response::HTTP_NOT_FOUND
                );
            }
            $model->delete();
            return $model;
        }  catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

  
}
