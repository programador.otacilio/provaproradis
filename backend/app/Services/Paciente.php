<?php
namespace App\Services;
use App\Exceptions\ValidacaoCustomizadaException;
use Core\Services\AApiService;
use Illuminate\Http\Response;
use App\Models\Paciente as PacienteModel;
use DB;

class Paciente extends AApiService
{
    public function __construct(PacienteModel $model)
    {
        parent::__construct($model);
    }

    public function todos(){
        return $this->getModel()->all();
    }
    
    public function checkExist($noCpf,$id = null){
        $model =  $this->getModel()->where('no_cpf',Helpers::somente_numero($noCpf));
         if($id!=null){
             $model = $model->where('co_paciente','<>',$id);
         }
         return $model->count() ? true : false;
    }
    
     private function trata_dados($request){
         if(isset($request['no_cpf'])){
             $request['no_cpf'] = Helpers::somente_numero($request['no_cpf']); 
         }
      
         return $request;
    }

    public function checkExistById($id){
        $model =  $this->getModel()->where('co_paciente',$id);
        return $model->count() ? true : false;
    }

    public function inserir($request)
    {
        try {
            DB::beginTransaction();
            $result = $this->getModel()->create($this->trata_dados($request->all()));
            DB::commit();
            return $result;
        }  catch (\HttpException $queryException) {
            DB::rollBack();
            throw $queryException;
        }
    }
    public function buscar($id)
    {
        return $this->getModel()->find($id);
    }
    public function atualizar($request,$id)
    {
        try {
            DB::beginTransaction();
            $model = $this->getModel()->find($id);
            if(!$model) {
                throw new ValidacaoCustomizadaException(
                    'Registro não encontrado',
                    Response::HTTP_NOT_FOUND
                );
            }
            $model->fill($this->trata_dados($request->all()));
            $model->save();
            DB::commit();

            return $model;

        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }
    public function deletar($id)
    {
        try {
            $model = $this->getModel()->find($id);
            if(!$model) {
                throw new ValidacaoCustomizadaException(
                    'Registro não encontrado',
                    Response::HTTP_NOT_FOUND
                );
            }
            $model->delete();
            return $model;
        }  catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

  
}
