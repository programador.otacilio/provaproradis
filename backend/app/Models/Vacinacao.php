<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class Vacinacao extends Pivot
{
    protected $table = 'tb_vacinacao';
    protected $primaryKey = 'co_vacinacao';
    
    protected $fillable = [
        'co_vacinacao',
        'dt_vacinacao',
        'co_paciente',
        'co_vacina',
    ];
    
    public function vacina()
    {
        return $this->hasOne(
            Vacina::class,
            'co_vacina',
            'co_vacina'
        );
    }
    public function paciente()
    {
        return $this->hasOne(
            Paciente::class,
            'co_paciente',
            'co_paciente'
        );
    }
}
