<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class Paciente extends Pivot
{
    protected $table = 'tb_paciente';
    protected $primaryKey = 'co_paciente';
    
    protected $fillable = [
        'co_paciente',
        'no_cpf',
        'no_nome',
    ];
}
