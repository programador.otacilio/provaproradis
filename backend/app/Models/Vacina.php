<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class Vacina extends Pivot
{
    protected $table = 'tb_vacina';
    protected $primaryKey = 'co_vacina';
    
    protected $fillable = [
        'co_vacina',
        'co_fabricante',
        'ds_lote',
        'dt_validade',
        'nu_dose',
        'nu_dias_intervalo'
    ];

    public function fabricante()
    {
        return $this->hasOne(
            Fabricante::class,
            'co_fabricante',
            'co_fabricante'
        );
        
    }
}
