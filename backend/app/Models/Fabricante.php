<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class Fabricante extends Pivot
{
    protected $table = 'tb_fabricante';
    protected $primaryKey = 'co_fabricante';
    
    protected $fillable = [
        'co_fabricante',
        'no_nome',
    ];
}
