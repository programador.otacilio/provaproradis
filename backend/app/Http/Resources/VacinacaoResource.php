<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VacinacaoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if(!isset($this->co_vacina)){
           return [];
        }
        return [
                'co_vacinacao'=>$this->co_vacinacao,
                'vacina'=>new VacinaResource($this->vacina),
                'paciente'=>new PacienteResource($this->paciente),
                
        ]; 
    }
}
