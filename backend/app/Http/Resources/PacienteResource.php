<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PacienteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if(!isset($this->co_paciente)){
           return [];
        }
        return [
                'co_paciente'=>$this->co_paciente,
                'no_cpf'=>$this->no_cpf,
                'no_nome'=>$this->no_nome,
                'identificacao'=>'['.$this->no_cpf.'] '.$this->no_nome
        ]; 
    }
}
