<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FabricanteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if(!isset($this->co_fabricante)){
           return [];
        }
        return [
                'id'=>$this->co_fabricante,
                'no_nome'=>$this->no_nome,
        ]; 
    }
}
