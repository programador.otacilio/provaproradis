<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Services\Helpers;
class VacinaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if(!isset($this->co_vacina)){
           return [];
        }
        return [
                'co_vacina'=>$this->co_vacina,
                'co_fabricante'=>$this->co_fabricante,
                'fabricante'=>$this->fabricante,
                'ds_lote'=>$this->ds_lote,
                'dt_validade'=>Helpers::dataBr($this->dt_validade),
                'dt_validade_us'=>$this->dt_validade,
                'nu_dose'=>$this->nu_dose,
                'nu_dias_intervalo'=>$this->nu_dias_intervalo,
                'identificacao'=>$this->fabricante->no_nome.'| lote '.$this->ds_lote
        ]; 
    }
}
