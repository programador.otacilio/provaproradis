<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Core\Http\Controllers\IApiResourceController;
use Illuminate\Http\Response;
use App\Services\Vacinacao as VacinacaoService;
use App\Http\Requests\VacinacaoRequest;
use App\Http\Resources\VacinacaoResource;
class VacinacaoController extends Controller implements IApiResourceController
{
    protected $service;

    public function __construct(VacinacaoService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        return $this->sendResponse(
            VacinacaoResource::collection($this->service->todos()),
            __('responses.success.item'),
            Response::HTTP_OK
        );
    }

    public function store(VacinacaoRequest $request)
    {
        return $this->sendResponse(
            $this->service->inserir($request),
            __('responses.success.create'),
            Response::HTTP_OK
        );
    }
    public function show($id)
    {   
        return $this->sendResponse(
            new VacinacaoResource($this->service->buscar($id)),
            __('responses.success.item'),
            Response::HTTP_OK
        );
    }
    public function update(VacinacaoRequest $request, $id)
    {   
        return $this->sendResponse(
            $this->service->atualizar($request,$id),
            __('responses.success.update'),
            Response::HTTP_OK
        );
    }
    public function destroy($id)
    {
        return $this->sendResponse(
            $this->service->deletar($id),
            __('responses.success.destroy'),
            Response::HTTP_OK
        );
    }
}
