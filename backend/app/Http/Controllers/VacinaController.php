<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Core\Http\Controllers\IApiResourceController;
use Illuminate\Http\Response;
use App\Services\Vacina as VacinaService;
use App\Http\Requests\VacinaRequest;
use App\Http\Resources\VacinaResource;
class VacinaController extends Controller implements IApiResourceController
{
    protected $service;

    public function __construct(VacinaService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        return $this->sendResponse(
            VacinaResource::collection($this->service->todos()),
            __('responses.success.item'),
            Response::HTTP_OK
        );
    }

    public function store(VacinaRequest $request)
    {
        return $this->sendResponse(
            $this->service->inserir($request),
            __('responses.success.create'),
            Response::HTTP_OK
        );
    }
    public function show($id)
    {   
        return $this->sendResponse(
            new VacinaResource($this->service->buscar($id)),
            __('responses.success.item'),
            Response::HTTP_OK
        );
    }
    public function update(VacinaRequest $request, $id)
    {   
        return $this->sendResponse(
            $this->service->atualizar($request,$id),
            __('responses.success.update'),
            Response::HTTP_OK
        );
    }
    public function destroy($id)
    {
        return $this->sendResponse(
            $this->service->deletar($id),
            __('responses.success.destroy'),
            Response::HTTP_OK
        );
    }
}
