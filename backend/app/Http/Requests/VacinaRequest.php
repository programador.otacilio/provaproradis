<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Services\Helpers;
use Illuminate\Validation\Rule;
use Core\Http\Requests\AFormRequest;



use Request;


class VacinaRequest extends AFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $sometimes = '';
        if(Request::route("id")!=null){
            $sometimes = 'sometimes'; 
        }

    	$rules = [
                'co_fabricante'=>[
                $sometimes,
                'required',      
            ],
            'ds_lote' => [
                $sometimes,
                'required',
            ],
            'dt_validade' => [
                $sometimes,
                'date_format:Y-m-d',
                'required',
            ],
            'nu_dose' => [
                $sometimes,
                'required',
            ],
            'nu_dias_intervalo' => [
                $sometimes,
                'required',
            ],
        ];

     
        
    	return $rules;
    }
    
    
    public function messages()
    {
    	return [
                'validation_cpf'=>'CPF inválido',
                'required' => 'O campo ":attribute" é obrigatório!',
                'date_format'=>'O campo ":attribute" é inválido!'
               
    	];
    }

   
}
