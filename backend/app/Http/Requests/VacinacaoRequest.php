<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Services\Helpers;
use Illuminate\Validation\Rule;
use Core\Http\Requests\AFormRequest;
use App\Rules\DoseIntervalo;
use App\Rules\DoseMaxima;
use App\Rules\DoseFabricante;

use Request;

class VacinacaoRequest extends AFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $sometimes = '';
        if(Request::route("id")!=null){
            $sometimes = 'sometimes'; 
        }
        
    	$rules = [
            'co_paciente'=>[
                $sometimes,
                'required',
                new DoseMaxima,      
                new DoseIntervalo,
                new DoseFabricante
            ],
            'co_vacina' =>[
                $sometimes,
                'required',
            ],
            'dt_vacinacao' =>[
                $sometimes,
                'date_format:Y-m-d',
                'required',
            ],
            
        ];

     
        
    	return $rules;
    }
    
    
    public function messages()
    {
    	return [
                'validation_cpf'=>'CPF inválido',
                'required' => 'O campo ":attribute" é obrigatório!',
                'date_format'=>'O campo ":attribute" é inválido!'
               
    	];
    }

   
}
