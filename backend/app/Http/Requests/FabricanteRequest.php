<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Services\Helpers;
use Illuminate\Validation\Rule;
use Core\Http\Requests\AFormRequest;


use Request;


class FabricanteRequest extends AFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	
    	
        $sometimes = '';
        if(Request::route("id")!=null){
            $sometimes = 'sometimes'; 
        }

    	$rules = [
            'no_nome'=>[
                $sometimes,
                'required',
                'string',  
            ],    
        ];

     
        
    	return $rules;
    }
    
    
    public function messages()
    {
    	return [
                'validation_cpf'=>'CPF inválido',
                'required' => 'O campo ":attribute" é obrigatório!',
               
    	];
    }

   
}
